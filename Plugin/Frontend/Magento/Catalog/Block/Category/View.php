<?php

namespace Kowal\CategoryPageDescriptionHide\Plugin\Frontend\Magento\Catalog\Block\Category;

class View
{

    /**
     * @var string
     */
    protected $_pageVarName = 'p';

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request
    )
    {
        $this->request = $request;
    }

    public function afterGetCurrentCategory(
        \Magento\Catalog\Block\Category\View $subject,
        $result
    )
    {
        $page = (int)$this->request->getParam($this->_pageVarName, false);
        if ($page > 1) {
            $result->setDescription('');
        }
        return $result;
    }
}